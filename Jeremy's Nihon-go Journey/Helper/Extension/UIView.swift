//
//  UIView.swift
//  Jeremy's Nihon-go Journey
//
//  Created by Jeremy Endratno on 7/20/22.
//

import Foundation
import UIKit

extension UIView {
    func cornerRadius(_ size: CGFloat) {
        self.layer.cornerRadius = size
    }
    
    func circleCornerRadius() {
        DispatchQueue.main.async {
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
    
    func basicShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.2
        self.layer.shadowColor = UIColor.black.withAlphaComponent(5).cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height: 3)
    }
    
    func lessShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.4
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height: 0.5)
    }
    
    func border(color: UIColor = .black, width: CGFloat = 1) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    func dashBorder(color: UIColor = .black) {
        DispatchQueue.main.async { [self] in
            let border = CAShapeLayer()
            let rect = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
            
            border.bounds = rect
            border.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
            border.fillColor = UIColor.clear.cgColor
            border.strokeColor = color.cgColor
            border.lineWidth = 2.5
            border.lineJoin = CAShapeLayerLineJoin.round
            border.lineDashPattern = [6,3]
            border.path = UIBezierPath(roundedRect: rect, cornerRadius: frame.height / 2).cgPath
            
            layer.addSublayer(border)
        }
    }
    
    func border2(color: UIColor = .black) {
        DispatchQueue.main.async { [self] in
            let border = CAShapeLayer()
            let rect = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
            
            border.bounds = rect
            border.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
            border.fillColor = UIColor.clear.cgColor
            border.strokeColor = color.cgColor
            border.lineWidth = 2.5
            border.path = UIBezierPath(roundedRect: rect, cornerRadius: frame.height / 2).cgPath
            
            layer.addSublayer(border)
        }
    }
    
    func removeBorder() {
        self.layer.borderWidth = 0
    }
    
    func addTapRecognizer(target: UIViewController, action: Selector?) {
        let tap = UITapGestureRecognizer(target: target, action: action)
        self.addGestureRecognizer(tap)
    }
}
