//
//  UIViewController.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/9/22.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title: String, message: String = "") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
