//
//  TestStorage.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/30/22.
//

import Foundation

extension QuestionStorage {
    static func getN5() -> [Question] {
        return [
            Question(
                hiragana: "(先週)　デパトーに　かいもの　いきます",
                question: "Select the correct hiragana for kanji above",
                answers: ["せんしゅ", "せんしゅう", "ぜんしゅ", "ぜんしゅう"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "ごはんの　(後)で　さんぽします",
                question: "Select the correct hiragana for kanji above",
                answers: ["つぎ", "うしろ", "まえ", "あと"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "もう　いちど　(言って)　ください",
                question: "Select the correct hiragana for kanji above",
                answers: ["いって", "きって", "まって", "たって"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "ちかくに　(山)が　あります",
                question: "Select the correct hiragana for kanji above",
                answers: ["かわ", "やま", "いけ", "うみ"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "この　ホテルは　へやが (多い)です",
                question: "Select the correct hiragana for kanji above",
                answers: ["すくない", "おおい", "せまい", "ひろい"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "ともだちと　いっしょに　(学校)に　いきます",
                question: "Select the correct hiragana for kanji above",
                answers: ["がこう", "がこお", "がっこう", "がっこお"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "えんぴつが　(六本)　あります",
                question: "Select the correct hiragana for kanji above",
                answers: ["ろくほん", "ろくぽん", "ろっほん", "ろっぽん"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "この　(新聞)は　いくらですか",
                question: "Select the correct hiragana for kanji above",
                answers: ["しんむん", "しんぶん", "しむん", "しぶん"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "かさは　(外)に　あります",
                question: "Select the correct hiragana for kanji above",
                answers: ["いえ", "なか", "そと", "にわ"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "けさ　(しゃわー)を　あびました",
                question: "Select the correct katakana for the hiragana above",
                answers: ["シャワー", "シャウー", "ツァワー", "ツァウー"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "コーヒーを　(のみました)",
                question: "Select the correct kanji for hiragana above",
                answers: ["欺みました", "飲みました", "欽みました", "飯みました"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "あたらし　(くるま)を　かいました",
                question: "Select the correct kanji for hiragana above",
                answers: ["軍", "東", "車", "里"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "この　ほうしは　(1000えん)です",
                question: "Select the correct kanji for hiragana above",
                answers: ["1000内", "1000皿", "1000川", "1000円"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "しゅくだいが　(はんぶん)　おわりました",
                question: "Select the correct kanji for hiragana above",
                answers: ["半分", "羊分", "羊介", "半介"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "きのう　たなかさんと　(あいました)",
                question: "Select the correct kanji for hiragana above",
                answers: ["見いました", "書いました", "会いました", "話いました"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "いもうとと　(おなじ)　ふくを　かいました",
                question: "Select the correct kanji for hiragana above",
                answers: ["同じ", "伺じ", "何じ", "向じ"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "あそこで　バスに　(         )",
                question: "Select the correct answer to fill the blank",
                answers: ["のりました", "つぎました", "あがりました", "はいりました"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "わたしの　へやは　この　(         )の　2かいです",
                question: "Select the correct answer to fill the blank",
                answers: ["エレベーター", "エアコン", "プール", "アパート"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "さとうさんは　ギターを　じょうずに　(         )",
                question: "Select the correct answer to fill the blank",
                answers: ["うたいます", "ひきます", "ききます", "あそびます"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "テーブルに　おさらと　はしを　(         )　ください",
                question: "Select the correct answer to fill the blank",
                answers: ["ならべて", "とって", "たべて", "おらって"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "けさ　そうじを　したから　へやは　(         )です",
                question: "Select the correct answer to fill the blank",
                answers: ["きれい", "きたない", "おかるい", "くらい"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "きょうは　500 (         )　およぎました",
                question: "Select the correct answer to fill the blank",
                answers: ["ど", "ばん", "メートル", "グラム"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "えきから　たいしかんまでの　(         )を　かいて　ください",
                question: "Select the correct answer to fill the blank",
                answers: ["しゃしん", "ちず", "てがみ", "きっぷ"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "うるさいから　テレビを　(         )　ください",
                question: "Select the correct answer to fill the blank",
                answers: ["けして", "つけて", "しめて", "あけて"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "きょうは　(         )が　ふって　います",
                question: "Select the correct answer to fill the blank",
                answers: ["くもり", "はれ", "かぜ", "ゆき"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "はこに　りんごが　(5)　あります",
                question: "Select the correct term to use",
                answers: ["よっつ", "いつつ", "むっつ", "ななつ"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "めがねは　つくえの　(Above)に　あります",
                question: "Select the correct term to use",
                answers: ["そば", "よこ", "した", "うえ"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "ここは　でぐちです。いりぐちは　あちらです",
                question: "Select the most similiar sentence",
                answers: ["あちらから　でて　ください", "あちらから　おりて　ください", "あちらから　はいって　ください", "あちらから　わたって　ください"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "まいばん　くにの　かぞくに　でんわします",
                question: "Select the most similiar sentence",
                answers: ["よるは　ときどき　くにの　かぞくに　でんわします", "あさは　ときどき　くにの　かぞくに　でんわします", "よるは　いつも　くにの　かぞくに　でんわします", "あさは　いつも　くにの　かぞくに　でんわします"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "この　まちは　ゆうめいな　たてものが　あります",
                question: "Select the most similiar sentence",
                answers: ["この　まちは　ゆうめいな　ビルが　あります", "この　まちは　ゆうめいな　おちゃが　あります", "この　まちは　ゆうめいな　ケーキが　あります", "この　まちは　ゆうめいな　こえんが　あります"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "その　えいがは　おもしろくなかったです",
                question: "Select the most similiar sentence",
                answers: ["その　えいがは　たのしかったです", "その　えいがは　つまらなかったです", "その　えいがは　みじかかったです", "その　えいがは　ながかったです"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "たんじょうびは　6がつ15にちです",
                question: "Select the most similiar sentence",
                answers: ["6がつ１５にちに　けっこんしました", "6がつ１５にちに　テストが　はじまりました", "6がつ１５にちに　うまれました", "6がつ１５にちに　くにえ　かえりました"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "にねんまえに　きょうとえ　いきました",
                question: "Select the most similiar sentence",
                answers: ["きのう　きょうとえ　いきました", "おととい　きょうとえ　いきました", "きょねん　きょうとえ　いきました", "おととし　きょうとえ　いきました"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "にほん(    )　ラーメンは　おいしいです",
                question: "Select the correct answer to fill the blank",
                answers: ["に", "の", "を", "え"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "わたしには　きょうだいが　ふたり　います。おとうと　(         )　いもうとです",
                question: "Select the correct answer to fill the blank",
                answers: ["は", "も", "と", "か"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "たなかさん　(         )　きのう　どこかに　でかけましたか",
                question: "Select the correct answer to fill the blank",
                answers: ["で", "は", "を", "に"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "つぎの　かどを　みぎ (         )　まがって　ください",
                question: "Select the correct answer to fill the blank",
                answers: ["が", "や", "か", "に"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "きのう、わたしは　ひとり　(         )　えいがを　みに　いきました",
                question: "Select the correct answer to fill the blank",
                answers: ["が", "を", "で", "は"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "きょう　パーティーが　ありましたから、たなかさん (         )　きて　ください",
                question: "Select the correct answer to fill the blank",
                answers: ["に", "も", "や", "で"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "この　ぼうしは　やまださん (         )　ですか",
                question: "Select the correct answer to fill the blank",
                answers: ["や", "は", "の", "か"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "えきまで　タクシーで　1000えん (         )　です",
                question: "Select the correct answer to fill the blank",
                answers: ["ぐらい", "など", "ごろ", "も"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "さようなら。また (         )",
                question: "Select the correct answer to fill the blank",
                answers: ["おととい", "きょう", "らいしゅう", "こんげつ"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "わたしの　ははは　50さいです。ちちは　55さいです。ははは　ちち　(         )　5さい　わかいです",
                question: "Select the correct answer to fill the blank",
                answers: ["から", "まで", "より", "のほうが"],
                correctAnswer: 3
            ),
            Question(
                hiragana: "たべる　(         )　てを　あらいましょう",
                question: "Select the correct answer to fill the blank",
                answers: ["まえに", "のまえに", "あとに", "のあとに"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "きょねん　とうきょうで　ゆきが　あまり　(         )",
                question: "Select the correct answer to fill the blank",
                answers: ["ふりませんでした", "ふりません", "ふりました", "ふります"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "みて　ください。ちいさな　さかなが　たくさん　(         )よ",
                question: "Select the correct answer to fill the blank",
                answers: ["およぎます", "泳ぎません", "およぎました", "およいで　います"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "A「その　カメラは　いいですね。どこで　かいましたか」\nB「いえ、これは　あにに　(         )」",
                question: "Select the correct answer to fill the blank",
                answers: ["あげました", "もらいました", "うりました", "かいました"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "たまごりょうりの　じょうずな　つくりかたを　(         )　よみました",
                question: "Select the correct answer to fill the blank",
                answers: ["なにに", "なにも", "なにかへ", "なにかで"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "本田「はい、本田です」\n北山「あ、北山花子です。すみません、(         )」\n本田「はい、ちょっと　まって　くださいね」",
                question: "Select the correct answer to fill the blank",
                answers: ["ひろこさんを　おねがいします", "ひろこさんを　ください", "ひろこさんと　はなしますか", "ひろこさんと　はなしませんか"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "A「すみません。くだもの　(       )(       )( ⭐️ )(       )か」\nB「こちらです」",
                question: "Arrange the answer to make a correct sentence and select the one with a star",
                answers: ["どこ", "あります", "は", "に"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "A「山下さんは？」\nB「となりの　へやで　(       )(       )( ⭐️ )(       )　して」",
                question: "Arrange the answer to make a correct sentence and select the one with a star",
                answers: ["れんしゅう", "の", "ギター", "を"],
                correctAnswer: 1
            ),
            Question(
                hiragana: "A「かいしゃ　(       )(       )( ⭐️ )(       )　いって　いますか」\nB「わたしは　あるいて　いって　います」",
                question: "Arrange the answer to make a correct sentence and select the one with a star",
                answers: ["で", "は", "へ", "なに"],
                correctAnswer: 4
            ),
            Question(
                hiragana: "A「ジョンさん、しゅくだいは　ぜんぶ　おわりましたか」\nB「いいえ、まだです。ここ　(       )(       )( ⭐️ )(       )、さいごの　もんだいが　むずかしいです」",
                question: "Arrange the answer to make a correct sentence and select the one with a star",
                answers: ["は", "かんたんでした", "が", "まで"],
                correctAnswer: 2
            ),
            Question(
                hiragana: "A「わたしは　この　ほんを　かいます。アンナさんは　どんな　ほんが　いいですか」\nB「わたしは　もう　すこし　(       )(       )( ⭐️ )(       )が　いいです」",
                question: "Arrange the answer to make a correct sentence and select the one with a star",
                answers: ["ほん", "かんたんな", "が", "にほんご"],
                correctAnswer: 2
            ),
        ]
    }
}
