//
//  UserDefaultStorage.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/15/22.
//

import Foundation
 
class UserDefaultStorage {
    private static let userDefaultStandart = UserDefaults.standard
    
    // MARK: Save
    static func save(key: String, data: Any?) {
        if let data = data {
            userDefaultStandart.set(data, forKey: key)
            print("User Default save with key \(key) has succeeded")
        } else {
            print("User Default save with key \(key) has failed, data found nil")
        }
    }
    
    static func saveModel<T: Codable>(key: String, data: T) {
        do {
            userDefaultStandart.set(try JSONEncoder().encode(data), forKey: key)
            print("User Default save with key \(key) has succeeded")
        } catch {
            print("User Default save with key \(key) has failed, error while encoding model: \(error.localizedDescription)")
        }
    }
    
    // MARK: - Get
    static func getModel<T: Codable>(key: String, response: T.Type) -> T? {
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
            let value = try? JSONDecoder().decode(T.self, from: data)
            return value
        }
        
        return nil
    }
    
    static func getString(key: String) -> String? {
        return userDefaultStandart.string(forKey: key)
    }
    
    static func getBool(key: String) -> Bool? {
        return userDefaultStandart.bool(forKey: key)
    }
    
    static func getInt(key: String) -> Int? {
        return userDefaultStandart.integer(forKey: key)
    }
    
    // MARK: - Remove
    static func remove(key: String) {
        userDefaultStandart.removeObject(forKey: key)
    }
}
