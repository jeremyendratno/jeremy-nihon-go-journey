//
//  Question.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/30/22.
//

import Foundation

struct Question {
    var hiragana: String = ""
    var romaji: String = ""
    var english: [String] = []
    var question: String = ""
    var answers: [String] = []
    var correctAnswer: Int = 0
    var isAnswered: Bool = false
}

extension [Question] {
    func getRandomUnansweredQuestion() -> Question? {
        var questions = self
        questions.removeAll(where: { $0.isAnswered })
        
        if questions.isEmpty {
            return nil
        } else {
            return questions.randomElement()
        }
    }
    
    mutating func resetQuestion() {
        for i in 0 ..< self.count {
            self[i].isAnswered = false
        }
    }
}
