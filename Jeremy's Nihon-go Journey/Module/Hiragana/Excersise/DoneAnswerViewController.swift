//
//  DoneAnswerViewController.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/15/22.
//

import UIKit

protocol DoneDelegate: AnyObject {
    func onDone()
    func onAgain()
}

class DoneAnswerViewController: UIViewController {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var wrongAnswerLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var againButton: UIButton!
    
    var wrongCount: Int = 0
    weak var delegate: DoneDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        backView.cornerRadius(20)
        backView.border(width: 2.5)
        doneButton.circleCornerRadius()
        doneButton.border(color: .black.withAlphaComponent(0.75), width: 2.5)
        againButton.circleCornerRadius()
        againButton.border(color: .black.withAlphaComponent(0.75), width: 2.5)
        
        if wrongCount == 0 {
            wrongAnswerLabel.text = "Perfectly Answered! No Wrong Answer"
        } else {
            wrongAnswerLabel.text = "\(wrongCount) Times Answered Wrong"
        }
    }
    
    @IBAction func doneButton(_ sender: Any) {
        delegate?.onDone()
        dismiss(animated: true)
    }
     
    @IBAction func againButton(_ sender: Any) {
        delegate?.onAgain()
        dismiss(animated: true)
    }
}
