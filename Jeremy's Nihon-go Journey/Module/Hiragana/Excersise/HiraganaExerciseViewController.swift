//
//  HiraganaExerciseViewController.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/15/22.
//

import UIKit
import AVFoundation

protocol ExerciseDelegate: AnyObject {
    func onFinish()
}

class HiraganaExerciseViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var progressBarView: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var hiraganaBackView: UIView!
    @IBOutlet weak var questionBackView: UIView!
    @IBOutlet weak var leftRopeView: UIView!
    @IBOutlet weak var rightRopeView: UIView!
    @IBOutlet weak var textFieldBackView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var hiraganaLabel: UILabel!
    
    var id: String = ""
    var questions: [Question] = []
    var wrongCount: Int = 0
    weak var delegate: ExerciseDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetup()
        dataSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
    }
    
    func viewSetup() {
        backButton.circleCornerRadius()
        backButton.border(width: 2.5)
        progressBarView.circleCornerRadius()
        progressBarView.border(width: 2.5)
        progressView.circleCornerRadius()
        progressView.border(width: 2.5)
        progressWidthConstraint.constant = 0
        hiraganaBackView.cornerRadius(20)
        hiraganaBackView.border(width: 2.5)
        questionBackView.cornerRadius(20)
        questionBackView.border(width: 2.5)
        leftRopeView.border(width: 2.5)
        leftRopeView.cornerRadius(3.75)
        rightRopeView.border(width: 2.5)
        rightRopeView.cornerRadius(3.75)
        textFieldBackView.circleCornerRadius()
        textFieldBackView.border(width: 2.5)
        textField.delegate = self
    }
    
    func dataSetup() {
        displayQuestion()
    }
    
    func displayQuestion() {
        if let question = questions.getRandomUnansweredQuestion() {
            hiraganaLabel.text = question.hiragana
        } else {
            if wrongCount == 0 {
                let perfect = (UserDefaultStorage.getInt(key: "\(id) Perfect") ?? 0) + 1
                UserDefaultStorage.save(key: "\(id) Perfect", data: perfect)
            }
            
            showDonePopup(wrongCount: wrongCount)
        }
    }
     
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let index = questions.firstIndex(where: { hiraganaLabel.text == $0.hiragana }) else { return false }
        
        if questions[index].romaji.simplify() == textField.text?.simplify() {
            questions[index].isAnswered = true
            displayQuestion()
        } else {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            showWrongPopup(answer: questions[index].romaji)
        }
        
        updateProgressBar()
        textField.text?.removeAll()
        return false
    }
    
    @IBAction func skipButton(_ sender: Any) {
        displayQuestion()
    }
    
    @IBAction func backButton(_ sender: Any) {
        delegate?.onFinish()
        navigationController?.popViewController(animated: true)
    }
}

extension HiraganaExerciseViewController: WrongDelegate, DoneDelegate {
    func onDone() {
        delegate?.onFinish()
        navigationController?.popViewController(animated: true)
    }
    
    func onAgain() {
        let attemp = (UserDefaultStorage.getInt(key: "\(id) Attemp") ?? 0) + 1
        UserDefaultStorage.save(key: "\(id) Attemp", data: attemp)
        wrongCount = 0
        updateProgressBar()
        questions.resetQuestion()
        displayQuestion()
    }
    
    func understand() {
        wrongCount += 1
        displayQuestion()
    }
}
