//
//  WrongAnswerViewController.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/15/22.
//

import UIKit

protocol WrongDelegate: AnyObject {
    func understand()
}

class WrongAnswerViewController: UIViewController {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var correctAnswer: UILabel!
    @IBOutlet weak var understandButton: UIButton!
    @IBOutlet weak var informationLabel: UILabel!
    
    var answer: String = "No Correct Answer"
    var isMeaning = false
    weak var delegate: WrongDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backView.cornerRadius(20)
        backView.border(width: 2.5)
        understandButton.circleCornerRadius()
        understandButton.border(color: .black.withAlphaComponent(0.75), width: 2.5)
        correctAnswer.text = answer
        
        if isMeaning {
            informationLabel.text = "The Correct Meaning:"
        }
    }
     
    @IBAction func understandButton(_ sender: Any) {
        delegate?.understand()
        dismiss(animated: true)
    }
}
