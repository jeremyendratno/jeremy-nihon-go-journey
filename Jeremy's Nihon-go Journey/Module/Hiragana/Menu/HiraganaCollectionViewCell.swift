//
//  HiraganaCollectionViewCell.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/15/22.
//

import UIKit

class HiraganaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var contentBackView: UIView!
    @IBOutlet weak var modeBackView: UIView!
    @IBOutlet weak var scoreBackView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var mascotLabel: UILabel!
    @IBOutlet weak var attempLabel: UILabel!
    @IBOutlet weak var perfectLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        modeBackView.border(color: .black.withAlphaComponent(0.5), width: 2.5)
        modeBackView.circleCornerRadius()
        self.border(color: .black.withAlphaComponent(0.75), width: 2.5)
        self.cornerRadius(20)
        scoreBackView.border(color: .black.withAlphaComponent(0.75), width: 2.5)
    }

    func setup(menu: HiraganaMenu) {
        titleLabel.text = menu.title
        descriptionLabel.text = menu.description
        mascotLabel.text = menu.mascotLetter
        attempLabel.text = "\(menu.attemp)"
        perfectLabel.text = "\(menu.perfect)"
        
        if menu.color == .green {
            contentBackView.backgroundColor = UIColor(named: "Light Green Main")
            modeBackView.backgroundColor = UIColor(named: "Green Main")
        } else if menu.color == .red {
            contentBackView.backgroundColor = UIColor(named: "Light Red Main")
            modeBackView.backgroundColor = UIColor(named: "Red Main")
        }
    }
}
