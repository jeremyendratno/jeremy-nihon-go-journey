//
//  HiraganaViewController.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/15/22.
//

import UIKit

class HiraganaViewController: UIViewController, ExerciseDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    
    let viewModel = HiraganaViewModel()
    var parentVC: TabbarViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewSetup()
    }
    
    func onFinish() {
        collectionView.reloadData()
    }
}

extension HiraganaViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionViewSetup() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "HiraganaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        collectionView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 94, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getMenus().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HiraganaCollectionViewCell
        cell.setup(menu: viewModel.getMenus()[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 40, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let menu = viewModel.getMenus()[indexPath.row]
        let attemp = (UserDefaultStorage.getInt(key: "\(menu.id) Attemp") ?? 0) + 1
        UserDefaultStorage.save(key: "\(menu.id) Attemp", data: attemp)
        
        let exerciseVC = HiraganaExerciseViewController()
        exerciseVC.questions = menu.questions
        exerciseVC.id = menu.id
        exerciseVC.delegate = self
        parentVC?.navigationController?.pushViewController(exerciseVC, animated: true)
    }
}
