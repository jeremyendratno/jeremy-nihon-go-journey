//
//  HiraganaViewModel.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/15/22.
//

import Foundation
import UIKit

struct HiraganaMenu {
    var id: String
    var title: String = "No Title"
    var description: String = "No Description"
    var mascotLetter: String = "0"
    var color: UIColor = .white
    var attemp: Int = 0
    var perfect: Int = 0
    var questions: [Question] = []
}

class HiraganaViewModel {
    func getMenus() -> [HiraganaMenu] {
        return [
            HiraganaMenu(
                id: "Simple Hiragana",
                title: "Hiragana",
                description: "Train your basic hiragana",
                mascotLetter: "あ",
                color: .green,
                attemp: UserDefaultStorage.getInt(key: "Simple Hiragana Attemp") ?? 0,
                perfect: UserDefaultStorage.getInt(key: "Simple Hiragana Perfect") ?? 0,
                questions: QuestionStorage.getBasicHiragana()
            ),
            HiraganaMenu(
                id: "Simple Katakana",
                title: "Katakana",
                description: "Train your basic katakana",
                mascotLetter: "ア",
                color: .red,
                attemp: UserDefaultStorage.getInt(key: "Simple Katakana Attemp") ?? 0,
                perfect: UserDefaultStorage.getInt(key: "Simple Katakana Perfect") ?? 0,
                questions: QuestionStorage.getBasicKatakana()
            ),
            HiraganaMenu(
                id: "Advance Hiragana",
                title: "Hiragana 2",
                description: "Train your advance hiragana",
                mascotLetter: "が",
                color: .green,
                attemp: UserDefaultStorage.getInt(key: "Advance Hiragana Attemp") ?? 0,
                perfect: UserDefaultStorage.getInt(key: "Advance Hiragana Perfect") ?? 0,
                questions: QuestionStorage.getAdvanceHiragana()
            ),
            HiraganaMenu(
                id: "Advance Katakana",
                title: "Katakana 2",
                description: "Train your advance katakana",
                mascotLetter: "ガ",
                color: .red,
                attemp: UserDefaultStorage.getInt(key: "Advance Katakana Attemp") ?? 0,
                perfect: UserDefaultStorage.getInt(key: "Advance Katakana Perfect") ?? 0,
                questions: QuestionStorage.getAdvanceKatakana()
            ),
            HiraganaMenu(
                id: "All Hiragana",
                title: "Hiragana All",
                description: "Train your hiragana for all level",
                mascotLetter: "あ", color: .green,
                attemp: UserDefaultStorage.getInt(key: "All Hiragana Attemp") ?? 0,
                perfect: UserDefaultStorage.getInt(key: "All Hiragana Perfect") ?? 0,
                questions: QuestionStorage.getAllHiragana())
            ,
            HiraganaMenu(
                id: "All Katakana",
                title: "Katakana All",
                description: "Train your katakana for all level",
                mascotLetter: "ア",
                color: .red,
                attemp: UserDefaultStorage.getInt(key: "All Katakana Attemp") ?? 0,
                perfect: UserDefaultStorage.getInt(key: "All Katakana Perfect") ?? 0,
                questions: QuestionStorage.getAllKatakana()),
        ]
    }
}
