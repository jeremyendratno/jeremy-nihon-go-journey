//
//  TabbarHelper.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/15/22.
//

import Foundation
import UIKit

extension TabbarViewController {
    func activateTab(backView: UIView, label: UILabel) {
        backView.backgroundColor = UIColor(named: "Yellow Main")
        backView.border(width: 2)
        label.textColor = .black
    }
    
    func deactivateTab(backView: UIView, label: UILabel) {
        backView.backgroundColor = .clear
        backView.removeBorder()
        label.textColor = .white
    } 
    
    func removeContentView() {
        contentView.subviews.forEach {
            $0.removeFromSuperview()
        }
    }
    
    func addContentView(viewController: UIViewController) {
        contentView.addSubview(viewController.view) 
    }
}
