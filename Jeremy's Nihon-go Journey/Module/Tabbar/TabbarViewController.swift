//
//  TabbarViewController.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/15/22.
//

import UIKit

class TabbarViewController: UIViewController {
    @IBOutlet weak var tabbarStackView: UIStackView!
    @IBOutlet weak var hiraganaBackView: UIView!
    @IBOutlet weak var hiraganaLabel: UILabel!
    @IBOutlet weak var vocabularyBackView: UIView!
    @IBOutlet weak var vocabularyLabel: UILabel!
    @IBOutlet weak var testBackView: UIView!
    @IBOutlet weak var testLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    
    let hiraganaVC = HiraganaViewController()
    let vocabularyVC = VocabularyViewController()
    let testVC = TestViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetup()
    }
    
    func viewSetup() {
        navigationController?.navigationBar.isHidden = true
        tabbarSetup()
        viewControllerSetup()
    }
    
    func tabbarSetup() {
        tabbarStackView.circleCornerRadius()
        tabbarStackView.border(width: 2)
        hiraganaBackView.circleCornerRadius()
        vocabularyBackView.circleCornerRadius()
        testBackView.circleCornerRadius()
        hiraganaBackView.addTapRecognizer(target: self, action: #selector(hiraganaTapped))
        vocabularyBackView.addTapRecognizer(target: self, action: #selector(vocabularyTapped))
        testBackView.addTapRecognizer(target: self, action: #selector(testTapped))
        activateTab(backView: hiraganaBackView, label: hiraganaLabel)
    }
    
    func viewControllerSetup() {
        DispatchQueue.main.async { [self] in
            hiraganaVC.view.frame.size = contentView.frame.size
            hiraganaVC.parentVC = self
            hiraganaVC.view.layer.name = "Hiragana"
            
            vocabularyVC.view.frame.size = contentView.frame.size
            vocabularyVC.parentVC = self
            vocabularyVC.view.layer.name = "Vocabulary"
            
            testVC.view.frame.size = contentView.frame.size
            testVC.parentVC = self
            testVC.view.layer.name = "Test"
            
            addContentView(viewController: hiraganaVC)
        }
    }
    
    func deactivateAllTab() {
        deactivateTab(backView: hiraganaBackView, label: hiraganaLabel)
        deactivateTab(backView: vocabularyBackView, label: vocabularyLabel)
        deactivateTab(backView: testBackView, label: testLabel)
    }
    
    @objc func hiraganaTapped() {
        deactivateAllTab()
        activateTab(backView: hiraganaBackView, label: hiraganaLabel)
        addContentView(viewController: hiraganaVC)
    }
    
    @objc func vocabularyTapped() {
        deactivateAllTab()
        activateTab(backView: vocabularyBackView, label: vocabularyLabel)
        addContentView(viewController: vocabularyVC)
    }
    
    @objc func testTapped() {
        deactivateAllTab()
        activateTab(backView: testBackView, label: testLabel)
        addContentView(viewController: testVC)
    }
}
