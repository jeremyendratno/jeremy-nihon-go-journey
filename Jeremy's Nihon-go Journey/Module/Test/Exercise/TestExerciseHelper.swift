//
//  TestExerciseHelper.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/30/22.
//

import Foundation

extension TestExerciseViewController {
    func updateProgressBar() {
        var answeredQuestionCount: Double = 0
        
        for question in questions {
            if question.isAnswered {
                answeredQuestionCount += 1
            }
        }
        
        let percentage = answeredQuestionCount / Double(questions.count)
        progressWidthConstraint.constant = progressBarView.frame.width * percentage
    }
    
    func showWrongPopup(answer: String) {
        let wrongVC = WrongAnswerViewController()
        wrongVC.modalPresentationStyle = .overCurrentContext
        wrongVC.modalTransitionStyle = .crossDissolve
        wrongVC.answer = answer
        wrongVC.delegate = self
        wrongVC.isMeaning = false
        present(wrongVC, animated: true)
    }
    
    func showDonePopup(wrongCount: Int) {
        let doneVC = DoneAnswerViewController()
        doneVC.modalPresentationStyle = .overCurrentContext
        doneVC.modalTransitionStyle = .crossDissolve
        doneVC.wrongCount = wrongCount
        doneVC.delegate = self
        present(doneVC, animated: true)
    }
}
