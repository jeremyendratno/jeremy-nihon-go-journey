//
//  TestExerciseViewController.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/30/22.
//

import UIKit
import AVFoundation

class TestExerciseViewController: UIViewController, NSLayoutManagerDelegate { 
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var progressBarView: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var hiraganaBackView: UIView!
    @IBOutlet weak var questionBackView: UIView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var leftRopeView: UIView!
    @IBOutlet weak var rightRopeView: UIView!
    @IBOutlet weak var answer1BackView: UIView!
    @IBOutlet weak var answer2BackView: UIView!
    @IBOutlet weak var answer3BackView: UIView!
    @IBOutlet weak var answer4BackView: UIView!
    @IBOutlet weak var answer1Label: UILabel!
    @IBOutlet weak var answer2Label: UILabel!
    @IBOutlet weak var answer3Label: UILabel!
    @IBOutlet weak var answer4Label: UILabel!
    @IBOutlet weak var hiraganaTextView: UITextView!
    
    var id: String = ""
    var questions: [Question] = []
    var wrongCount: Int = 0
    weak var delegate: ExerciseDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetup()
        displayQuestion()
    }
    
    func viewSetup() {
        hiraganaTextView.layoutManager.delegate = self
        backButton.circleCornerRadius()
        backButton.border(width: 2.5)
        progressBarView.circleCornerRadius()
        progressBarView.border(width: 2.5)
        progressView.circleCornerRadius()
        progressView.border(width: 2.5)
        progressWidthConstraint.constant = 0
        hiraganaBackView.border(width: 2.5)
        questionBackView.border(width: 2.5)
        leftRopeView.border(width: 2.5)
        leftRopeView.cornerRadius(3.75)
        rightRopeView.border(width: 2.5)
        rightRopeView.cornerRadius(3.75)
        answer1BackView.border(width: 2.5)
        answer1BackView.cornerRadius(20)
        answer2BackView.border(width: 2.5)
        answer2BackView.cornerRadius(20)
        answer3BackView.border(width: 2.5)
        answer3BackView.cornerRadius(20)
        answer4BackView.border(width: 2.5)
        answer4BackView.cornerRadius(20)
        answer1BackView.addTapRecognizer(target: self, action: #selector(answer1Tapped))
        answer2BackView.addTapRecognizer(target: self, action: #selector(answer2Tapped))
        answer3BackView.addTapRecognizer(target: self, action: #selector(answer3Tapped))
        answer4BackView.addTapRecognizer(target: self, action: #selector(answer4Tapped))
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, shouldBreakLineByWordBeforeCharacterAt charIndex: Int) -> Bool {
        guard let string = layoutManager.textStorage?.string as? String else { return false }
        if charIndex == 0 { return false }
        
        if string[charIndex - 1] == "　" || string[charIndex - 1] == "。" || string[charIndex - 1] == "、" {
            return true
        } else {
            return false
        }
    }
    
    func dataSetup() {
        displayQuestion()
    }
    
    func displayQuestion() {
        if let question = questions.getRandomUnansweredQuestion() {
            hiraganaTextView.text = question.hiragana
            questionLabel.text = question.question
            answer1Label.text = question.answers[0]
            answer2Label.text = question.answers[1]
            answer3Label.text = question.answers[2]
            answer4Label.text = question.answers[3]
        } else {
            if wrongCount == 0 {
                let perfect = (UserDefaultStorage.getInt(key: "\(id) Perfect") ?? 0) + 1
                UserDefaultStorage.save(key: "\(id) Perfect", data: perfect)
            }
            
            showDonePopup(wrongCount: wrongCount)
        }
    }
    
    @objc func answer1Tapped() {
        answerTapped(answer: 1)
    }
    
    @objc func answer2Tapped() {
        answerTapped(answer: 2)
    }
    
    @objc func answer3Tapped() {
        answerTapped(answer: 3)
    }
    
    @objc func answer4Tapped() {
        answerTapped(answer: 4)
    }
    
    func answerTapped(answer: Int) {
        guard let index = questions.firstIndex(where: { hiraganaTextView.text == $0.hiragana }) else { return }
        
        if questions[index].correctAnswer == answer {
            questions[index].isAnswered = true
            displayQuestion()
        } else {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            showWrongPopup(answer: questions[index].answers[questions[index].correctAnswer - 1])
        }
        
        updateProgressBar()
    } 
    
    @IBAction func skipButton(_ sender: Any) {
        displayQuestion()
    }
    
    @IBAction func backButton(_ sender: Any) {
        delegate?.onFinish()
        navigationController?.popViewController(animated: true)
    }
}

extension TestExerciseViewController: WrongDelegate, DoneDelegate {
    func onDone() {
        delegate?.onFinish()
        navigationController?.popViewController(animated: true)
    }
    
    func onAgain() {
        let attemp = (UserDefaultStorage.getInt(key: "\(id) Attemp") ?? 0) + 1
        UserDefaultStorage.save(key: "\(id) Attemp", data: attemp)
        wrongCount = 0
        questions.resetQuestion()
        updateProgressBar()
        displayQuestion()
    }
    
    func understand() {
        wrongCount += 1
        displayQuestion()
    }
} 
