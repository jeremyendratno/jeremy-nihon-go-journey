//
//  TestCollectionViewCell.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/30/22.
//

import UIKit

class TestCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var contentBackView: UIView!
    @IBOutlet weak var scoreBackView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var attempLabel: UILabel!
    @IBOutlet weak var perfectLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.border(color: .black.withAlphaComponent(0.75), width: 2.5)
        self.cornerRadius(20)
        scoreBackView.border(color: .black.withAlphaComponent(0.75), width: 2.5)
    }

    func setup(menu: TestMenu) {
        titleLabel.text = menu.title
        descriptionLabel.text = menu.description 
        attempLabel.text = "\(menu.attemp)"
        perfectLabel.text = "\(menu.perfect)"
    }
}
