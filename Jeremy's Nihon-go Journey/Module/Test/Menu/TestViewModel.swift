//
//  TestViewModel.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/30/22.
//

import Foundation

struct TestMenu {
    var id: String
    var title: String = "No Title"
    var description: String = "No Description" 
    var attemp: Int = 0
    var perfect: Int = 0
    var questions: [Question] = []
}

class TestViewModel {
    func getMenus() -> [TestMenu] {
        return [
            TestMenu(
                id: "N5",
                title: "N5",
                description: "Tryout JLPT N5 test example",
                attemp: UserDefaultStorage.getInt(key: "N5 Attemp") ?? 0,
                perfect: UserDefaultStorage.getInt(key: "N5 Perfect") ?? 0,
                questions: QuestionStorage.getN5())
        ]
    }
}
