//
//  VocabularyViewModel.swift
//  日本語's Journey
//
//  Created by Jeremy Endratno on 11/15/22.
//

import Foundation
import UIKit

struct VocabularyMenu {
    var id: String
    var title: String = "No Title"
    var description: String = "No Description"
    var mascotImage: UIImage? = nil
    var color: UIColor = .blue
    var attemp: Int = 0
    var perfect: Int = 0
    var questions: [Question] = []
}

class VocabularyViewModel {
    func getMenus() -> [VocabularyMenu] {
        return [
            VocabularyMenu(
                id: "Vocabulary Object",
                title: "Object",
                description: "Vocabulary for object around you",
                mascotImage: UIImage(systemName: "desktopcomputer"),
                color: .blue,
                attemp: UserDefaultStorage.getInt(key: "Vocabulary Object Attemp") ?? 0,
                perfect: UserDefaultStorage.getInt(key: "Vocabulary Object Perfect") ?? 0,
                questions: QuestionStorage.getObject()
            ),
            VocabularyMenu(
                id: "Vocabulary Verb",
                title: "Verb",
                description: "Vocabulary for verb that you use everyday",
                mascotImage: UIImage(systemName: "figure.walk"),
                color: .green,
                attemp: UserDefaultStorage.getInt(key: "Vocabulary Verb Attemp") ?? 0,
                perfect: UserDefaultStorage.getInt(key: "Vocabulary Verb Perfect") ?? 0,
                questions: QuestionStorage.getVerb()
            ),
            VocabularyMenu(
                id: "Vocabulary Adverb",
                title: "Adverb",
                description: "Vocabulary for adverb to spicify things",
                mascotImage: UIImage(systemName: "flame.fill"),
                color: .red,
                attemp: UserDefaultStorage.getInt(key: "Vocabulary Adverb Attemp") ?? 0,
                perfect: UserDefaultStorage.getInt(key: "Vocabulary Adverb Perfect") ?? 0,
                questions: QuestionStorage.getAdverb()
            ),
        ]
    }
}
